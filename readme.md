## Nodes
### HTTPblock
The HTTPBlock takes in a digilines message containing a URL and then returns the content of it. 

```
digiline_send("httpblock", "http://example.com/textfile.txt")
```

will return somthing like this:

```

<!doctype html>
<html>
<body>
<div>
    <h1>Example Domain</h1>
    <p>This domain is for use in illustrative examples in documents. You may use this
    domain in literature without prior coordination or asking for permission.</p>
    <p><a href="https://www.iana.org/domains/example">More information...</a></p>
</div>
</body>
</html>

```

This is a script which returns raw HTML and sends it to a processing block:

```lua
if event.type ==  "on" or event.type == "program" then
    digiline_send("httpblock", "http://example.com")
end

if event.type == "digiline" and event.channel == "lol" then
    digiline_send("processor", event.msg)
end
```

### Selector
**Note** This node is not usable; Implementation is mostly done except the actual selecting bit.

The selector takes in HTML and a selector: 

```
digiline_send([[
    <li id='breaking'>Breaking: The server enables the HTTPBlock mod, making the world an IOT device!</li>
    <li>The boring player does yet another thing</li>
    <li id='breaking'>The server goes into recession. Sell your houses before they become worthless!</li>}},
    "#breaking"
)
```

To those familiar with Jquery, this returns the same text as `$("#breaking").html()` would.

You will then recieve a table which contains the contents of all HTML elements which match:

```
{
    "Breaking: The server enables the HTTPBlock mod, making the world an IOT device!",
    "Breaking: The server goes into recession. Sell your houses before they become worthless!"
}
```

### Poster
Send a table containing the URL and data to post:

```
digiline_send("poster", {
    url = "thing.com",
    data = "stuff"
})
```

When the post request is done, you will recieve the error code. If it's not 200, something's gone wrong.
ContentDB
## Installation
Install like any other mod, but ensure you add this mod to trusted mods or Minetest probably won't let you send HTTP requests.

## Roadmap
Currently, the mod only contains the `httpblock:httpblock`. However, there are plans for more complicated blocks now that the Minetest interfacing code is mostly done:

Here are the ones which are 75% the way there but need a little more effort to become stable:

* HTML Query block - give it table containing HTML and a query (like `#id` or `.class`), and it will return the contents of all elements matching the query. (i am currently working on this as we speak)
* HTTP Posters - sends a post request

And here are the hypothetical ones:

* JSON converters - turns JSON into a lua table for use later
* Image-shower - Takes in images and turns them into a format suitable for use with the digilines screen

There are also plans for a web-based service to allow Minetest to interface with, allowing for admins to have an API to interface with scripts. For example, the server wiki might have a script scraping the first news item and sending it to endpoint `127.0.0.1/news.txt` for the mod to send to LCD screens across the server.

In any case, if i don't make these before 2023-10-01, feel free to make them yourself.

## Credits
The code and textures are a lightly modified version of the digilines RTC - credit to the devs!

## Beurocracy
The license for the code is LGPL and the license for the image is CC0.

