local httpblock_nodebox =
{
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16, 8/16, -7/16, 8/16 }, -- bottom slab

		{ -7/16, -7/16, -7/16, 7/16, -5/16,  7/16 },
	}
}

local httpblock_selbox =
{
	type = "fixed",
	fixed = {{ -8/16, -8/16, -8/16, 8/16, -3/16, 8/16 }}
}

-- ChatGPT made this function
function extractContent(html, selector)
    local result = {}

    local function extract(node)
        if node.type == "element" then
            if selector == node.tag or (selector:sub(1, 1) == "#" and selector:sub(2) == node.id) then
                local content = ""
                for _, child in ipairs(node.children) do
                    if child.type == "text" then
                        content = content .. child.value
                    elseif child.type == "element" then
                        content = content .. extract(child)
                    end
                end
                table.insert(result, content)
            else
                for _, child in ipairs(node.children) do
                    extract(child)
                end
            end
        end
    end

    local function parseTag(tagStr)
        local tag = {}
        tag.tag = tagStr:match("<%s*(%w+)")
        tag.id = tagStr:match("#(%w+)")
        tag.children = {}
        for childStr in tagStr:gmatch("<(.-)>") do
            local child = parseTag(childStr)
            if child then
                table.insert(tag.children, child)
            else
                local text = childStr:match(">(.-)<")
                if text then
                    table.insert(tag.children, { type = "text", value = text })
                end
            end
        end
        return tag
    end

    local rootTagStr = html:match("<html>(.-)</html>")
    local rootTag = parseTag(rootTagStr)
    extract(rootTag)

    return result
end


local on_digiline_receive = function (pos, _, channel, msg)
	local setchan = minetest.get_meta(pos):get_string("channel")
	if channel == setchan then  
	    print(msg)
	    --print(msg.html)
	    print(msg.selector)
	
	    if msg.html and msg.selector then -- TODO: Make this work!
    		contents = extractContent(msg.html, msg.selector)
    		
    		if content then
        		for i, value in ipairs(content) do
                    print(i, value)
                end
            else
                print("Nothing returned from selector " .. msg.selector)
    		end
    		
	    	digilines.receptor_send(pos, digilines.rules.default, channel, contents)
	    end
	end
end

minetest.register_node("httpblock:select_block", {
	description = "HTML selector block",
	drawtype = "nodebox",
	tiles = {"selectblock.png"},

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {dig_immediate=2},
	selection_box = httpblock_selbox,
	node_box = httpblock_nodebox,
	digilines =
	{
		receptor = {},
		effector = {
			action = on_digiline_receive
		},
	},

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,

	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end,
})

minetest.register_craft({
	output = "httpblock:httpblock",
	recipe = {
		{"dye:white", "dye:green", "dye:white"},
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"},
		{"", "digilines:wire_std_00000000", ""}
	}
})
